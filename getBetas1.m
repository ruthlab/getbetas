%%%         get betas from excel sheet of BV data  %%% 
%order of empathy protocol conditions: instructions(1), sensad(2),
% sad(3), senhappy(4), happy(5), sennetral(6), neutral(7),senanxiety (8),
% anxiety(9), sen scrambled(10), scrambled(11).

%%% Make sure the all_betas_021218.xlsx file is in the working directory %%

%read betas from excel sheet
[B,Btxt,Braw] = xlsread('all_betas_021218');

condition_names = ["Instruction" "Sad" "Happy" "Neutral" "Anxiety" "Scrambled"];

Regionnames={Btxt{1,:}};
%B=B(2:61,:);
%subjectN=number of subjects
subjectN=size(B,1);
regionCount = size(B,2);
happyVec=zeros(subjectN,regionCount);
sadVec=zeros(subjectN,regionCount);
neutralVec=zeros(subjectN,regionCount);
anxietyVec=zeros(subjectN,regionCount);
scrambledVec=zeros(subjectN,regionCount);
emotionsVec = zeros(subjectN,6,regionCount);
endpoint= length(B);

for regionIndex = 1:regionCount
    for subjectIndex = 3 : 11 : endpoint
        emotionsVec(ceil(subjectIndex / 11),1, regionIndex) = B(subjectIndex,regionIndex);
        emotionsVec(ceil(subjectIndex / 11),2, regionIndex) = B(subjectIndex + 2,regionIndex);
        emotionsVec(ceil(subjectIndex / 11),3, regionIndex) = B(subjectIndex + 4,regionIndex);
        emotionsVec(ceil(subjectIndex / 11),4, regionIndex) = B(subjectIndex + 6,regionIndex);
        emotionsVec(ceil(subjectIndex / 11),5, regionIndex) = B(subjectIndex + 8,regionIndex);
    end
    % define group (1=SC, 2=KC)
    emotionsVec(1:31, 6, regionIndex) = 1;
    emotionsVec(32:60, 6, regionIndex) = 2;
end

ROI_Emotion = mean(emotionsVec(:, [1 2 4]), 2);
ROI_NonE = mean(emotionsVec(:, [3 5]), 2);

% Allocating space for the csv output structure
formated_output = zeros(661,6,20); 

for condition_index = 1:size(condition_names)
    % The titles in order for the first row formatted as region_condition
    title_strings = strcat(Btxt(1,3:end), '_', condition_names(condition_index)); 
    formated_output(1,condition_index,:) = title_strings;
    % The betas from the second row and on
    formated_output(2:end,condition_index,:) = emotionsVec(:,condition_index,:);
end

% imported function to write strings to csv
cell2csv('output_avg_betas_ROIs_allsubjects.csv', formated_output);
%csvwrite('output_avg_betas_ROIs_allsubjects.csv', emotionsVec);

% t-test: assuming unequal variance, two tail (no dierctionality assumed)
% p value=0.005
%[h,p,ci,stats]=ttest2(Kdata,Sdata,'vartype','unequal','tail','both')


%%% Boxplots

for regionIndex = 1:regionCount
maxVal = max(emotionsVec(:,1,regionIndex)) + 0.2;
minVal = min(emotionsVec(:,1,regionIndex)) - 0.2;

f = figure;
p = uipanel('Parent',f,'BorderType','none','Title', Regionnames(regionIndex+1),...
    'TitlePosition','centertop'); 
% p.FontSize = 12;
% p.FontWeight = 'bold';
% p.BackgroundColor='white';

subplot(1,2,1,'Parent',p);
a = boxplot(emotionsVec(emotionsVec(:,6,1) == 1,1,regionIndex));
ylim([minVal maxVal]);
xlabel('SC')

ylabel('Betas')
subplot(1,2,2,'Parent',p);
boxplot(emotionsVec(emotionsVec(:,6,1) == 2,1,regionIndex));
ylim([minVal maxVal]);
xlabel('KC')

end

